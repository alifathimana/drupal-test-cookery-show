<?php
/**
 * @file
 * Contains \Drupal\score\Controller\Display.
 */

namespace Drupal\testimonial\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class Display.
 *
 * @package Drupal\my_custom\Controller
 */
class Hellocontroller extends ControllerBase {

  /**
   * showdata.
   *
   * @return string
   *   Return Table format data.
   */
  public function list() {
   
// you can write your own query to fetch the data I have given my example.

    $result = \Drupal::database()->select('testimonial', 'n')
            ->fields('n', array('id', 'name', 'email', 'description'))
            ->execute()->fetchAllAssoc('id');
// Create the row element.
    $rows = array();
    foreach ($result as $row => $detail) {
      $rows[] = array(
        'data' => array($detail->id, $detail->name, $detail->email, $detail->description));
    }
// Create the header.
    $header = array('Sl No.', 'name', 'email', 'description');
    $output = array(
      '#theme' => 'table',    // Here you can write #type also instead of #theme.
      '#header' => $header,
      '#rows' => $rows
    );
    return $output;
  }
  
}
